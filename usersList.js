export default UsersComponent = {
    template: `
        <div class="usersList">

            <ul>

                <li v-for="(user , index) in reverseUsers" :key="index">

                    {{ user.name }} ||

                    <button @click="editUser(index)">Edit</button>

                    <button @click="deleteUser(index)">Delete</button>

                </li>

            </ul>

        </div>
    `,
    props: ['campaigns', 'domainphoto']
}